package com.anhj.coinwashmanager.entity;

import com.anhj.coinwashmanager.interfaces.CommonModelBuilder;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class UsageDetails {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "machineId", nullable = false)
    private Machine machine;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "memberId", nullable = false)
    private Member member;

    @Column(nullable = false)
    private LocalDateTime dateUsage;

    private UsageDetails(UsageDetailsBuilder builder) {
        this.machine = builder.machine;
        this.member = builder.member;
        this.dateUsage = builder.dateUsage;

    }

    public static class UsageDetailsBuilder implements CommonModelBuilder<UsageDetails> {
        private final Machine machine;
        private final Member member;
        private final LocalDateTime dateUsage;

        public UsageDetailsBuilder(Machine machine, Member member, LocalDateTime dateUsage) {
                // request가 필요 없음. "1번 회원이 5번 기계를 2001년5월5일에 사용했다!" 이기때문에 바로 받아야 함
            this.machine = machine;
            this.member = member;
            this.dateUsage = dateUsage;
        }


        @Override
        public UsageDetails build() {
            return new UsageDetails(this);
        }
    }
}
