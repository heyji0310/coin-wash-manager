package com.anhj.coinwashmanager.entity;

import com.anhj.coinwashmanager.enums.MachineType;
import com.anhj.coinwashmanager.interfaces.CommonModelBuilder;
import com.anhj.coinwashmanager.model.MachineNameUpdateRequest;
import com.anhj.coinwashmanager.model.MachineRequest;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Machine {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Enumerated(value = EnumType.STRING)
    @Column(nullable = false, length = 10)
    private MachineType machineType;

    @Column(nullable = false, length = 15)
    private String machineName;

    @Column(nullable = false)
    private LocalDate datePurchase;

    @Column(nullable = false)
    private Double machinePrice;

    // 사용중인지 체크 필요 -> 이유 : 삭제하면 안되기때문에, 회원가입에서도 탈퇴했더라도 내용 삭제는 할 수 없다.
    //                            이용 내역을 꼭 유지해야 하기 때문에
    // @Column(nullable = false)
    // private Boolean isUsed;

    @Column(nullable = false)
    private LocalDateTime dateCreate;

    @Column(nullable = false)
    private LocalDateTime dateUpdate;

    public void putDataName(MachineNameUpdateRequest updateRequest) {
        this.machineName = updateRequest.getMachineName();
    }

    private Machine(MachineBuilder builder) {
        this.machineType = builder.machineType;
        this.machineName = builder.machineName;
        this.datePurchase = builder.datePurchase;
        this.machinePrice = builder.machinePrice;
        this.dateCreate = builder.dateCreate;
        this.dateUpdate = builder.dateUpdate;
    }

    public static class MachineBuilder implements CommonModelBuilder<Machine> {
        private final MachineType machineType;
        private final String machineName;
        private final LocalDate datePurchase;
        private final Double machinePrice;
        private final LocalDateTime dateCreate;
        private final LocalDateTime dateUpdate;

        public MachineBuilder(MachineRequest request) {
            this.machineType = request.getMachineType();
            this.machineName = request.getMachineName();
            this.datePurchase = request.getDatePurchase();
            this.machinePrice = request.getMachinePrice();
            this.dateCreate = LocalDateTime.now();
            this.dateUpdate = LocalDateTime.now();
        }

        @Override
        public Machine build() {
            return new Machine(this);
        }
    }
}
