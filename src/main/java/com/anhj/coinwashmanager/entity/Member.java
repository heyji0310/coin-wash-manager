package com.anhj.coinwashmanager.entity;

import com.anhj.coinwashmanager.interfaces.CommonModelBuilder;
import com.anhj.coinwashmanager.model.MemberJoinRequest;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Member {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, length = 20)
    private String memberName;

    @Column(nullable = false, length = 20)
    private String memberPhone;

    @Column(nullable = false)
    private LocalDate birthday;

    @Column(nullable = false)
    private Boolean isEnable;

    @Column(nullable = false)
    private LocalDateTime dateJoin;

    private LocalDateTime dateWithdrawal;

    public void putWithdrawal() {
        this.isEnable = false;
        this.dateWithdrawal = LocalDateTime.now();
    }


    private Member(MemberBuilder builder) {
        this.memberName = builder.memberName;
        this.memberPhone = builder.memberPhone;
        this.birthday = builder.birthday;
        this.isEnable = builder.isEnable;
        this.dateJoin = builder.dateJoin;
    }

    public static class MemberBuilder implements CommonModelBuilder<Member> {
        private final String memberName;
        private final String memberPhone;
        private final LocalDate birthday;
        private final Boolean isEnable;
        private final LocalDateTime dateJoin;

        public MemberBuilder(MemberJoinRequest joinRequest) {
            this.memberName = joinRequest.getMemberName();
            this.memberPhone = joinRequest.getMemberPhone();
            this.birthday = joinRequest.getBirthday();
            this.isEnable = true; // 회원가입 하는 순간 강제로 true
            this.dateJoin = LocalDateTime.now();
        }

        @Override
        public Member build() {
            return new Member(this);
        }
    }
}
