package com.anhj.coinwashmanager.model;

import com.anhj.coinwashmanager.entity.Member;
import com.anhj.coinwashmanager.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class MemberItem {
    @ApiModelProperty(notes = "회원 시퀀스")
    private Long id;
    @ApiModelProperty(notes = "회원이름")
    private String memberName;
    @ApiModelProperty(notes = "회원 연락처")
    private String memberPhone;
    @ApiModelProperty(notes = "회원 생일")
    private LocalDate birthday;
    @ApiModelProperty(notes = "유효회원인가")
    private String isEnable;
    @ApiModelProperty(notes = "가입일시")
    private LocalDateTime dateJoin;
    @ApiModelProperty(notes = "탈퇴일시")
    private LocalDateTime dateWithdrawal;

    private MemberItem(MemberItemBuilder builder) {
        this.id = builder.id;
        this.memberName = builder.memberName;
        this.memberPhone = builder.memberPhone;
        this.birthday = builder.birthday;
        this.isEnable = builder.isEnable;
        this.dateJoin = builder.dateJoin;
        this.dateWithdrawal = builder.dateWithdrawal;
    }

    public static class MemberItemBuilder implements CommonModelBuilder<MemberItem> {
        private final Long id;
        private final String memberName;
        private final String memberPhone;
        private final LocalDate birthday;
        private final String isEnable;
        private final LocalDateTime dateJoin;
        private final LocalDateTime dateWithdrawal;

        public MemberItemBuilder(Member member) {
            this.id = member.getId();
            this.memberName = member.getMemberName();
            this.memberPhone = member.getMemberPhone();
            this.birthday = member.getBirthday();
            this.isEnable = member.getIsEnable() ? "예" : "아니오";  // 삼항연산자 사용 -> true면 "예", false면 "아니오"
            this.dateJoin = member.getDateJoin();
            this.dateWithdrawal = member.getDateWithdrawal();
        }

        @Override
        public MemberItem build() {
            return new MemberItem(this);
        }
    }
}
