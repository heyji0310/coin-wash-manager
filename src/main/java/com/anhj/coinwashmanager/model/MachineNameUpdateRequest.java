package com.anhj.coinwashmanager.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MachineNameUpdateRequest {
    @ApiModelProperty(notes = "기계이름")
    private String machineName;
}
