package com.anhj.coinwashmanager.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Getter
@Setter
public class MemberJoinRequest {
    @ApiModelProperty(notes = "회원 이름")
    @NotNull
    @Length(min = 2, max = 20)
    private String memberName;

    @ApiModelProperty(notes = "회원 연락처")
    @NotNull
    @Length(max = 20)
    private String memberPhone;

    @ApiModelProperty(notes = "회원 생일")
    @NotNull
    private LocalDate birthday;
}
