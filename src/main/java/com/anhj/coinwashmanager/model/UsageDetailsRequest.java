package com.anhj.coinwashmanager.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Getter
@Setter
public class UsageDetailsRequest {
    @ApiModelProperty(notes = "회원 id", required = true)
    @NotNull
    private Long memberId;

    @ApiModelProperty(notes = "기계 id", required = true)
    @NotNull
    private Long machineId;

    @ApiModelProperty(notes = "이용시간", required = true)
    @NotNull
    private LocalDateTime dateUsage;
}
