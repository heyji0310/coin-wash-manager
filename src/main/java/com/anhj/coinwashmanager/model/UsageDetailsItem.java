package com.anhj.coinwashmanager.model;

import com.anhj.coinwashmanager.entity.UsageDetails;
import com.anhj.coinwashmanager.interfaces.CommonModelBuilder;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class UsageDetailsItem {
    @ApiModelProperty(notes = "이용내역 시퀀스")
    private Long usageDetailsId;
    @ApiModelProperty(notes = "이용일자")
    private LocalDateTime dateUsage;

    @ApiModelProperty(notes = "기계 시퀀스")
    private Long machineId;

    @ApiModelProperty(notes = "기계 전체 이름(타입, 이름)")
    private String machineFullName;

    @ApiModelProperty(notes = "회원 시퀀스")
    private Long memberId;

    @ApiModelProperty(notes = "회원 정보(이름, 연락처, 생년월일)")
    private String memberFUllName;

    @ApiModelProperty(notes = "유효회원")
    private Boolean memberIsEnable;

    @ApiModelProperty(notes = "회원 가입 일자")
    private LocalDateTime memberDateJoin;

    @ApiModelProperty(notes = "회원 탈퇴 일자")
    private LocalDateTime memberDateWithdrawal;

    private UsageDetailsItem(UsageDetailsItemBuilder builder) {
        this.usageDetailsId = builder.usageDetailsId;
        this.dateUsage = builder.dateUsage;
        this.machineId = builder.machineId;
        this.machineFullName = builder.machineFullName;
        this.memberId = builder.memberId;
        this.memberFUllName = builder.memberFullName;
        this.memberIsEnable = builder.memberIsEnable;
        this.memberDateJoin = builder.memberDateJoin;
        this.memberDateWithdrawal = builder.memberDateWithdrawal;
    }

    public static class UsageDetailsItemBuilder implements CommonModelBuilder<UsageDetailsItem> {
        private final Long usageDetailsId;
        private final LocalDateTime dateUsage;

        private final Long machineId;
        private final String machineFullName;

        private final Long memberId;
        private final String memberFullName;
        private final Boolean memberIsEnable;
        private final LocalDateTime memberDateJoin;
        private final LocalDateTime memberDateWithdrawal;

        public UsageDetailsItemBuilder(UsageDetails usageDetails) {
            this.usageDetailsId = usageDetails.getId();
            this.dateUsage = usageDetails.getDateUsage();
            this.machineId = usageDetails.getMachine().getId();
            this.machineFullName = "[" + usageDetails.getMachine().getMachineType().getName() + "], " + usageDetails.getMachine().getMachineName();
            this.memberId = usageDetails.getMember().getId();
            this.memberFullName = "[" + usageDetails.getMember().getBirthday().getYear() + "년생]" + usageDetails.getMember().getMemberName() + ", " + usageDetails.getMember().getMemberPhone();
            this.memberIsEnable = usageDetails.getMember().getIsEnable();
            this.memberDateJoin = usageDetails.getMember().getDateJoin();
            this.memberDateWithdrawal = usageDetails.getMember().getDateWithdrawal();
        }

        @Override
        public UsageDetailsItem build() {
            return new UsageDetailsItem(this);
        }
    }
}
