package com.anhj.coinwashmanager.model;

import com.anhj.coinwashmanager.enums.MachineType;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Getter
@Setter
public class MachineRequest {
    @ApiModelProperty(notes = "기계타입", required = true)
    @NotNull
    @Enumerated(value = EnumType.STRING)
    private MachineType machineType;

    @ApiModelProperty(notes = "기계이름", required = true)
    @NotNull
    @Length(max = 15)
    private String machineName;

    @ApiModelProperty(notes = "구매일", required = true)
    @NotNull
    private LocalDate datePurchase;

    @ApiModelProperty(notes = "기계가격", required = true)
    @NotNull
    private Double machinePrice;
}
