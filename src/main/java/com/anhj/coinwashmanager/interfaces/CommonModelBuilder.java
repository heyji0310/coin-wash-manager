package com.anhj.coinwashmanager.interfaces;

  // interface는 할일을 정해주는 것
public interface CommonModelBuilder<T> { // 제네릭 : 무언가 , 반복적인 작업을 줄일 때 사용 / <T> 타입
    T build();
}
