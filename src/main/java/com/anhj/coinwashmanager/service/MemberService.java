package com.anhj.coinwashmanager.service;

import com.anhj.coinwashmanager.entity.Member;
import com.anhj.coinwashmanager.exception.CMissingDataException;
import com.anhj.coinwashmanager.exception.CNoMemberDataException;
import com.anhj.coinwashmanager.model.ListResult;
import com.anhj.coinwashmanager.model.MemberItem;
import com.anhj.coinwashmanager.model.MemberJoinRequest;
import com.anhj.coinwashmanager.repository.MemberRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class MemberService {
    private final MemberRepository memberRepository;

    public Member getMemberData(long id) {
        return memberRepository.findById(id).orElseThrow(CMissingDataException::new);
    }

    public void setMember(MemberJoinRequest joinRequest) { // 회원 등록
        Member member = new Member.MemberBuilder(joinRequest).build();
        memberRepository.save(member);
    }

    public ListResult<MemberItem> getMembers() {  // 전체 회원 조회
        List<Member> members = memberRepository.findAll();

        List<MemberItem> result = new LinkedList<>();

        members.forEach(member -> {
            MemberItem addItem = new MemberItem.MemberItemBuilder(member).build();
            result.add(addItem);
        });

        return ListConvertService.settingResult(result);
    }

    public ListResult<MemberItem> getMembers(boolean isEnable) {  // isEnable이 true인 회원 정보 조회
        List<Member> members = memberRepository.findAllByIsEnableOrderByIdDesc(isEnable);

        List<MemberItem> result = new LinkedList<>();

        members.forEach(member -> {
            MemberItem addItem = new MemberItem.MemberItemBuilder(member).build();
            result.add(addItem);
        });

        return ListConvertService.settingResult(result);
    }

    public void putMemberWithdrawal(long id) { // 회원 탈퇴
        Member member = memberRepository.findById(id).orElseThrow(CMissingDataException::new);

        if (!member.getIsEnable()) throw new CNoMemberDataException(); // ! -> IsEnable 이 true가 아니면 exception을 보여줘라

        member.putWithdrawal();
        memberRepository.save(member);
    }
}
