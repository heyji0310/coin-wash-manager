package com.anhj.coinwashmanager.service;

import com.anhj.coinwashmanager.entity.Machine;
import com.anhj.coinwashmanager.entity.Member;
import com.anhj.coinwashmanager.entity.UsageDetails;
import com.anhj.coinwashmanager.model.ListResult;
import com.anhj.coinwashmanager.model.UsageDetailsItem;
import com.anhj.coinwashmanager.repository.UsageDetailsRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class UsageDetailsService {
    private final UsageDetailsRepository usageDetailsRepository;

    public void setUsageDetails(Member member, Machine machine, LocalDateTime dateUsage) {
        UsageDetails usageDetails = new UsageDetails.UsageDetailsBuilder(machine, member, dateUsage).build();
        usageDetailsRepository.save(usageDetails);
    }

    public ListResult<UsageDetailsItem> getAllUsageDetails() {
        List<UsageDetails> usageDetails = usageDetailsRepository.findAll();

        List<UsageDetailsItem> result = new LinkedList<>();

        usageDetails.forEach(usageDetail -> {
            UsageDetailsItem addItem = new UsageDetailsItem.UsageDetailsItemBuilder(usageDetail).build();
            result.add(addItem);
        });

        return ListConvertService.settingResult(result);
    }

    public ListResult<UsageDetailsItem> getUsageDetails(LocalDate dataStart, LocalDate dataEnd) {
        LocalDateTime dateStartTime = LocalDateTime.of(
                dataStart.getYear(),
                dataStart.getMonthValue(),
                dataStart.getDayOfMonth(),
                0,
                0,
                0
        );

        LocalDateTime dateEndTime = LocalDateTime.of(
                dataEnd.getYear(),
                dataEnd.getMonthValue(),
                dataEnd.getDayOfMonth(),
                23,
                59,
                59
        );

        List<UsageDetails> usageDetails = usageDetailsRepository.findAllByDateUsageGreaterThanEqualAndDateUsageLessThanEqualOrderByIdDesc(dateStartTime, dateEndTime);

        List<UsageDetailsItem> result = new LinkedList<>();

        usageDetails.forEach(usageDetail -> {
            UsageDetailsItem addItem = new UsageDetailsItem.UsageDetailsItemBuilder(usageDetail).build();
            result.add(addItem);
        });

        return ListConvertService.settingResult(result);
    }
}
