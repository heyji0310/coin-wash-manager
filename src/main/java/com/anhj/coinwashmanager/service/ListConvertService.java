package com.anhj.coinwashmanager.service;

import com.anhj.coinwashmanager.model.ListResult;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ListConvertService { // Convert : 전환하다 / int pageNum : 몇번째 페이지를 가져오고 싶은지
    public static PageRequest getPageable(int pageNum) {  //  리턴 타입이 PageRequest 여야 한다 / Pageable 페이지들을 원하는 만큼 묶어서 나눌 수 있다
        return PageRequest.of(pageNum - 1, 10);  //  10 원하는 만큼 묶음 통상적으로 10개씩 묶어준다
    }

    //  위와 같은 이름이 있는데 에러가 안나는 이유는 위에는 int pageNum 만 필요하지만 아래에는 int pageSize 까지 필요하기 때문
    public static PageRequest getPageable(int pageNum, int pageSize) {  //  위의 getPageable 와 동일하므로 getPageable 를 복붙해준다
        return PageRequest.of(pageNum - 1, pageSize);  //  pageSize 를 안적으면 10개씩 묶어주지만 적으면 내가 원하는 만큼 묶어준다
    }

    public static <T> ListResult<T> settingResult(List<T> list) {  //  무언가의 list 를 받았다 / (List<T> list) 리스트를 통으로 가져온다
        ListResult<T> result = new ListResult<>();
        result.setList(list);
        result.setTotalItemCount((long)list.size());  //  int 형이 기본설정 이지만 model ListResult 에서 long 형으로 지정되었으므로 long 으로 설정
        result.setTotalPage(1);  //  리스트를 통으로 받아서 한꺼번에 처리 하기 때문에 전체 페이지 는 1
        result.setCurrentPage(1);  //  리스트를 통으로 받아서 한꺼번에 처리 하기 때문에 현재 페이지 도 1

        return result;

    }

    public static <T> ListResult<T> settingResult(List<T> list, long totalItemCount, int totalPage, int currentPage) {
        ListResult<T> result = new ListResult<>();
        result.setList(list);
        result.setTotalItemCount(totalItemCount);
        //  페이지는 0부터 시작하지만 표시 해줄때는 1부터 표시 해줘야 한다
        result.setTotalPage(totalPage == 0 ? 1 : totalPage);  //  토탈 페이지가 0 일 경우 1로 표시해주고 아닐경우 페이지 번호를 적어준다
        result.setCurrentPage(currentPage + 1);  //  페이지의 시작은 0부터 이지만 표시는 1부터 해야하기 때문에 현재 페이지 + 1

        return result;
    }
}