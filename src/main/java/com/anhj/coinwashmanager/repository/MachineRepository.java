package com.anhj.coinwashmanager.repository;

import com.anhj.coinwashmanager.entity.Machine;
import com.anhj.coinwashmanager.enums.MachineType;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface MachineRepository extends JpaRepository<Machine, Long> {
    List<Machine> findAllByMachineTypeOrderByIdDesc(MachineType machineType);
}
