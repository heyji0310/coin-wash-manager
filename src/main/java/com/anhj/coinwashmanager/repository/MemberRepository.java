package com.anhj.coinwashmanager.repository;

import com.anhj.coinwashmanager.entity.Member;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface MemberRepository extends JpaRepository<Member, Long> {
    List<Member> findAllByIsEnableOrderByIdDesc(Boolean isEnable);
}
