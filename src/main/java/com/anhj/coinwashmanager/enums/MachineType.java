package com.anhj.coinwashmanager.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum MachineType {
    WASHER("세탁기")
    , DRYER("건조기")
    ;

    private final String name;
}
