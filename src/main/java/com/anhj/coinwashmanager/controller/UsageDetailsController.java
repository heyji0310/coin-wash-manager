package com.anhj.coinwashmanager.controller;

import com.anhj.coinwashmanager.entity.Machine;
import com.anhj.coinwashmanager.entity.Member;
import com.anhj.coinwashmanager.model.CommonResult;
import com.anhj.coinwashmanager.model.ListResult;
import com.anhj.coinwashmanager.model.UsageDetailsItem;
import com.anhj.coinwashmanager.model.UsageDetailsRequest;
import com.anhj.coinwashmanager.service.MachineService;
import com.anhj.coinwashmanager.service.MemberService;
import com.anhj.coinwashmanager.service.ResponseService;
import com.anhj.coinwashmanager.service.UsageDetailsService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.LocalDate;

@Api(tags = "이용내역 관리")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/usage-details")
public class UsageDetailsController {
    private final MemberService memberService;
    private final MachineService machineService;
    private final UsageDetailsService usageDetailsService;

    @ApiOperation(value = "이용내역 등록")
    @PostMapping("/new")
    public CommonResult setUsageDetails(@RequestBody @Valid UsageDetailsRequest request) {
        Member member = memberService.getMemberData(request.getMemberId());
        Machine machine = machineService.getMachineData(request.getMachineId());

        usageDetailsService.setUsageDetails(member, machine, request.getDateUsage());

        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "기간 내 이용내역 리스트 조회")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "dateStart", value = "시작일", required = true),
            @ApiImplicitParam(name = "dateEnd", value = "종료일", required = true)
    })
    @GetMapping("/search")
    public ListResult<UsageDetailsItem> getUsageDetails(
            @RequestParam(value = "dateStart") @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate dateStart,
            @RequestParam(value = "dateEnd") @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate dateEnd
    ) {
        return ResponseService.getListResult(usageDetailsService.getUsageDetails(dateStart, dateEnd), true);
    }

}
