package com.anhj.coinwashmanager.controller;

import com.anhj.coinwashmanager.model.CommonResult;
import com.anhj.coinwashmanager.model.ListResult;
import com.anhj.coinwashmanager.model.MemberItem;
import com.anhj.coinwashmanager.model.MemberJoinRequest;
import com.anhj.coinwashmanager.service.MemberService;
import com.anhj.coinwashmanager.service.ResponseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Api(tags = "회원 정보 관리")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/member")
public class MemberController {
    private final MemberService memberService;

    @ApiOperation(value = "회원 정보 등록")
    @PostMapping("/new")
    public CommonResult setMember(@RequestBody @Valid MemberJoinRequest joinRequest) {
        memberService.setMember(joinRequest);
        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "회원 탈퇴여부에 따른 결과 조회")
    @GetMapping("/search")
    public ListResult<MemberItem> getMembers(@RequestParam(value = "isEnable", required = false) Boolean isEnable) {
        if (isEnable == null) {  // 조회 조건 중 유효회원 조건을 입력하지 않으면
            return ResponseService.getListResult(memberService.getMembers(), true); // 전체 회원을 조회하고,
        } else {  // 조회 조건이 true면 유효회원을, false면 유효하지 않은 회원을 조회한다.
            return ResponseService.getListResult(memberService.getMembers(isEnable), true); // true 일 때 유효회원을 조회한다.
        }
    }

    @ApiOperation(value = "회원정보 삭제")
    @ApiImplicitParams(
            @ApiImplicitParam(name = "id", value = "회원 시퀀스", required = true)
    )
    @DeleteMapping("/{id}") // Delete Mapping 에 put 매서드 , 유효하지 않은 회원으로 전환
    public CommonResult delMember(@PathVariable long id) {
        memberService.putMemberWithdrawal(id);
        return ResponseService.getSuccessResult();
    }
}
