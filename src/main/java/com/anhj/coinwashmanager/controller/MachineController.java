package com.anhj.coinwashmanager.controller;

import com.anhj.coinwashmanager.enums.MachineType;
import com.anhj.coinwashmanager.model.*;
import com.anhj.coinwashmanager.service.MachineService;
import com.anhj.coinwashmanager.service.ResponseService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Api(tags = "기계 정보 관리")
@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/machine")
public class MachineController {
    private final MachineService machineService;

    @ApiOperation(value = "기계 정보 등록")
    @PostMapping("/new")
    public CommonResult setMachine(@RequestBody @Valid MachineRequest request) {
        machineService.setMachine(request);
        return ResponseService.getSuccessResult();
    }

    @ApiOperation(value = "기계 정보 조회")
    @ApiImplicitParams(
            @ApiImplicitParam(name = "id", value = "기계 시퀀스", required = true)
    )
    @GetMapping("/{id}")
    public SingleResult<MachineDetail> getMachine(@PathVariable long id) {
        return ResponseService.getSingleResult(machineService.getMachine(id));
    }

    @ApiOperation(value = "기계타입별 정보 조회")
    @GetMapping("/search")
    public ListResult<MachineItem> getMachines(@RequestParam(value = "machineType", required = false) MachineType machineType) {
        if (machineType == null) {
            return ResponseService.getListResult(machineService.getMachines(), true);
        } else {
            return ResponseService.getListResult(machineService.getMachines(machineType), true);
        }
    }

    @ApiOperation(value = "기계 정보 수정")
    @ApiImplicitParams(
            @ApiImplicitParam(name = "id", value = "기계 시퀀스", required = true)
    )
    @PutMapping("/{id}")
    public CommonResult putMachineName(@PathVariable long id, @RequestBody @Valid MachineNameUpdateRequest updateRequest) {
        machineService.putMachineName(id, updateRequest);
        return ResponseService.getSuccessResult();
    }
}
